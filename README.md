### Basics

1. [Fizz Buzz](https://blog.codinghorror.com/why-cant-programmers-program/)

    Write a program that prints the numbers from 1 to 100. But for multiples of three print "Fizz" instead of the number and for multiples of five print "Buzz". For numbers which are multiples of both three and five print "FizzBuzz".

### JavaScript

2. [Closures](http://bonsaiden.github.io/JavaScript-Garden/#function.closures)

    What is the output of the following code snippet?
    ```JavaScript
    for (var i = 0; i < 10; i++) {
        setTimeout(function() {
            console.log(i);  
        }, 1000);
    }
    ```
    How would you fix this? Identify as many ways/solutions as you can.
3. Implement `Array#map` and `Array#filter` using `Array#reduce`. ([Source](https://github.com/timoxley/functional-javascript-workshop))
    ```JavaScript
    var reduce = Array.prototype.reduce;

    function map(array, fn) {
        //...
    }
    function filter(array, fn) {
        //...
    }

    map([1,2,3], function(i) { return i*2; });
    filter([1,2,3], function(i) { return i%2 === 1; });
    ```

### Promises/A+

4. Implement `readFileAsync` using `fs.readFile` and `Promise` constructor.
    ```TypeScript
    var readFile : (
            filename: string,
            options: string | {encoding: string},
            callback: (err: Error, data: Buffer | string) => void
        ) => void
        = fs.readFile;
    class Promise {
        constructor(fn: (resolve: fn, reject: fn) => void): Promise<any> {
            //...
        }
    }


    function readFileAsync(
        filename: string,
        options: string | {encoding: string}
    ) : Promise<Buffer | string>
    {
        //...
    }
    ```
5. Implement the generic case---a `promisify` function that takes any node-style async function and returns a promisified interface to that function.
    ```TypeScript
    interface NodeStyleCallback {
        (err: Error, result: any): void;
    }
    interface NodeStyleAsyncFn {
        (...args: any[], fn: NodeStyleCallback): void;
    }

    interface PromisifiedeAsyncFn {
        (...args: any[]): Promise<result: any>;
    }


    function promisify(fn: NodeStyleAsyncFn): PromisifiedAsyncFn {
        //...
    }
    ```

### 